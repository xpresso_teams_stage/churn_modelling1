"""
This is a sample hello world flask app
It only has a root resource which sends back hello World html text
"""

import logging
import pickle
import os
import pandas as pd
from sklearn.preprocessing import StandardScaler
from sklearn.metrics import confusion_matrix
from keras.models import load_model
from xpresso.ai.core.data.inference import AbstractInferenceService
from xpresso.ai.core.logging.xpr_log import XprLogger

__author__ = "#Anushree#"

logger = XprLogger("inference",level=logging.INFO)


class Inference(AbstractInferenceService):
    """ Main class for the inference service. User will need to implement
    following functions:
       - load_model: It gets the directory of the model stored as parameters,
           user will need to implement the method for loading the model and
           updating self.model variable
       - transform_input: It gets the JSON object from the rest API as input.
           User will need to implement the method to convert the json data to
           relevant feature vector to be used for prediction
       - predict: It gets the feature vector or any other object from the
           transform_input method. User will need to implement the model
           prediction codebase here. It returns the predicted value
       - transform_output: It gets the predicted value from the predict method.
           User need to implement this method to converted predicted method
           to JSON serializable object. Response from this method is send back
           to the client as JSON Object.

    AbstractInferenceService automatically creates the flask reset api
    with resource /predict for this class.
    Request JSON format:
       {
         "input" : <input_goes_here> // It could be any JSON object
       }
       This value of "input" key is sent to transform_input

    Response JSON format:
       {
         "message": "success/failure",
         "results": <response goes here> // It could be any JSOn object
       }
       Output of transform_output goes as value of "results"
    """

    def __init__(self):
        super().__init__()
        self.model=None
        self.x_test=None
        self.y_test=None
        """ Initialize any static data required during boot up """

    def load_model(self, model_path):
        """ This is used to load the model on the boot time.
        User will need to load the model and save it in the variable
        self.model. It is IMPORTANT to update self.model.
        Args:
            model_path(str): Path of the directory where the model files are
               stored.
        """
        xdata_path=os.path.join(model_path, "/test_data/xtest.csv")
        ydata_path = os.path.join(model_path, "/test_data/ytest.csv")
        my_model_path=os.path.join(model_path, "churn_xbg.pkl")
        self.model = pickle.load(open(my_model_path,'rb'))
        self.x_test=pd.read_csv(xdata_path)
        self.y_test=pd.read_csv(ydata_path)
        print("Model Loaded")
        pass

    def transform_input(self, input_request):
        """
        Convert the raw input request to a feature data or any other object
        to be used during prediction

        Args:
            input_request: JSON Object or an array received from the REST API,
               which needs to be converted into relevant feature data.

        Returns:
            obj: Any transformed object
        """
        self.x_test=self.x_test.iloc[:,1:]
        sc=StandardScaler()
        self.x_test = sc.fit_transform(self.x_test)
        print(self.x_test.shape)

        self.y_test = self.y_test.iloc[:,1:]
        print(self.y_test.shape)
        return [self.x_test, self.y_test]


    def get_credentials(self):
        return {
            "uid": "asarkar",
            "pwd": "Abzooba@123",
            "env": "qa"
        }

    def predict(self, input_request):
        """
        This method implements the prediction method of the supported model.
        It gets the output of transform_input as input and returns the predicted
        value

        Args:
            input_request: Transformed object outputted from transform_input
               method

        Returns:
            obj: predicted value from the model

        """
        Y_true = input_request[1]
        Y_pred = self.model.predict(input_request[0])
        Y_pred = (Y_pred > 0.5)

        # Find Confusion matrix
        cm = confusion_matrix(Y_true, Y_pred)
        test_count = input_request[1].shape[0]
        arr = []
        for i in range(len(cm)):
            for j in range(len(cm[i])):
                if i == j:
                    arr.append(cm[i][j])

        accuracy = sum(arr) / test_count
        print(f'Accuracy {accuracy}')
        output_y = pd.DataFrame(Y_pred)
        output_y.to_csv("y_pred.csv")
        print("Saved")
        return Y_pred


    def transform_output(self, output_response):
        """
        Convert the predicted value into a relevant JSON serializable object.
        This is sent back to the REST API call
        Args:
            output_response: Predicted output from predict method.

        Returns:
          obj: JSON Serializable object
        """
        print("Print List")
        return output_response.tolist()


if __name__ == "__main__":
    pred = Inference()
    # === To run locally. Use load_model instead of load. ===
    # pred.load_model(model_path="../data")
    pred.load()
    pred.run_api(port=5000)